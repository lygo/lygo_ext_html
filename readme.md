# HTML utilities #
lygo_html has some utilities for parsing HTML and crawling web sites.

## How to Use

To use just call:

```
go get -u bitbucket.org/lygo/lygo_ext_html@v0.1.8
```

### Versioning

Sources are versioned using git tags:

```
git tag v0.1.8
git push origin v0.1.8
```