module bitbucket.org/lygo/lygo_ext_html

go 1.16

require (
	bitbucket.org/lygo/lygo_commons v0.1.120
	bitbucket.org/lygo/lygo_events v0.1.10
	github.com/google/uuid v1.3.0 // indirect
	golang.org/x/net v0.0.0-20210908191846-a5e095526f91
)
